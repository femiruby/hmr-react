import app from "./server";
import http from 'http';

const server = http.createServer(app);

let oldApp = app
server.listen(1000)
if (module.hot) {
    module.hot.accept('./server', () => {
        server.removeListener('request', oldApp)
        server.on('request', app)
        oldApp = app
    })
}

// if(module.hot){
//     module.hot.accept('./route.js', async () => {
//         console.log(4444)
//         Object.keys(require.cache).forEach(function(key) {
//             if(key.includes('route.js')) {
//                 console.log(key, 'yep')
//                 delete require.cache[key]
//             }
//         })
//         route = require('./route.js')
//         route()
//         server.removeListener('request', app)
//         server.on('request', app);
//         route(app)
//
//         hotApp = app;
//     })
// }
