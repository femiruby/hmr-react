import express from 'express';
import React from 'react';
import ReactDomServer from 'react-dom/server';
import {ServerApp, routes} from '../App';
import path from 'path'
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';
import webpack from 'webpack';
import webpackConfig from '../../webpack.config'
import {matchPath} from 'react-router';
import http from "http";
import router from "./route";

const webpackServer = webpackConfig.find(f => f.target === "node");
const webpackClient = webpackConfig.find(f => f.target === "web");


const clientCompiler = webpack(webpackClient);
const serverCompiler = webpack(webpackServer);

// serverCompiler.watch({}, () => {
//     console.log('yessss')
// })


const handlePublicRoute = (req, res, next) => {
    console.log(123)
}

const app = express();
app.use('/public', express.static('public/'), function (err, req, res, next) {
    console.error(err.stack)
    res.status(500).send('Something broke!')
});

const renderer = (req, res, next) => {
const context = {}
const renderedApp = ReactDomServer.renderToString(<ServerApp location={req.url} context={context}/>);

console.log(context, 'context123', req.url)
    const match = routes.filter(f => f.path === req.url);
    if(match.length === 0) return res.send('not found', 404);

const html = `
<!DOCTYPE html>
<head>
<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
</head>
<body>
    <div id="root">${renderedApp}</div>    
</body>
<script src="public/main.bundle.js"></script>
`;
res.send(html);
}

app.use(webpackDevMiddleware(clientCompiler, { noInfo: true, publicPath: webpackClient.output.publicPath}));
app.use(webpackHotMiddleware(clientCompiler))
// app.use(webpackDevMiddleware(serverCompiler, {noInfo: false, publicPath: "http://localhost:1000"}))
// app.use(webpackHotMiddleware(serverCompiler))

app.use('/jim', router);
app.get('*', renderer);

// const server = http.createServer(app);
// server.listen(1000)
// let oldApp = app
//
serverCompiler.watch({}, ()=>{
    console.log('reloading yeh')
    app.use('/jim', router);
})



// const server = http.createServer(app);
// let hotApp = app;
// server.listen(1000, function () {
//     console.log('listening on 10002')
// })

export default app;



