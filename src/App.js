import React from 'react';
import {createStore, combineReducers, applyMiddleware} from "redux";
import {Provider} from 'react-redux';
import {RootComponent, rootReducer} from "component/Root";
import reduxLogger from "redux-logger";
import {Switch, Route, StaticRouter, BrowserRouter} from "react-router-dom";

const reducer = combineReducers({root:rootReducer})
const store = createStore(reducer, applyMiddleware(reduxLogger));

export const routes = [
    {
        path: "/home",
        component: RootComponent,
        exact: true,
    }
]

export const ClientApp = () => {
    return (
        <Provider store={store}>
            <BrowserRouter>
                <Switch>
                    {routes.map(f => {
                        return (
                            <Route
                                component={f.component}
                                path={f.path}
                                exact={f.exact}
                            />
                        )
                    })}
                </Switch>
            </BrowserRouter>
        </Provider>
    )
}

export const ServerApp = ({location, context}) => {
    return (
        <Provider store={store}>
            <StaticRouter location={location} context={context}>
                <Switch>
                    {routes.map(f => {
                        return (
                            <Route
                                component={f.component}
                                path={f.path}
                                exact={f.exact}
                            />
                        )
                    })}
                </Switch>
            </StaticRouter>
        </Provider>
    )
}

