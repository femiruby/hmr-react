import React from 'react';
import ReactDom from 'react-dom';
import {ClientApp} from '../App'

ReactDom.hydrate(<ClientApp/>, document.getElementById('root'))

