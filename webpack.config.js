const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const webpack = require('webpack')
const nodeExternals = require('webpack-node-externals')
const startServerPlugin = require('start-server-webpack-plugin')

const webpackModule = {
    rules: [
        {
            test: /\.css$/,
            use: [
                'style-loader',
                'css-loader',
            ]
        },
        {
            test: /\.js$/,
            include: path.resolve('src'),
            use: ['babel-loader']
        },
        {
            test: /\.(jpg|jpeg|gif|png|ico)$/,
            exclude: /node_modules/,
            loader:'file-loader'
        }
    ]
}

const resolve = {
    alias: {
        component: path.resolve('src', 'client', 'component'),
    }
};

const devtool = "inline-source-map";

const plugins = [
    new webpack.HotModuleReplacementPlugin(),
];

const mode = "development";

const client = {
    devtool,
    resolve,
    module:webpackModule,
    plugins: [
        ...plugins,
        new CleanWebpackPlugin({
            cleanOnceBeforeBuildPatterns: [`*.hot-update.*`],
            dry: false,
            dangerouslyAllowCleanPatternsOutsideProject: true
        })
    ],
    mode,
    target:"web",
    entry: ['react-hot-loader/patch','webpack-hot-middleware/client','./src/client/index.js'],
    mode: 'development',
    output: {
        path: path.resolve( 'public'),
        chunkCallbackName: '[name].bundle.js',
        filename: "[name].bundle.js",
        publicPath: 'http://localhost:1000/public/',
        hotUpdateChunkFilename: 'hot/[hash].hot-update.js',
        hotUpdateMainFilename: 'hot/[hash].hot-update.json'
    },
}

const server = {
    module:webpackModule,
    resolve,
    devtool,
    plugins: [
        ...plugins,
    ],
    mode,
    target:"node",
    externals:[nodeExternals({
        whitelist: ['webpack/hot/poll?1000']
    })],
    entry:['webpack/hot/poll?1000', './src/index.js'],
    output: {
        path: path.resolve('public'),
        filename: "server.bundle.js",
        hotUpdateChunkFilename: 'hot/[hash].hot-update.js',
        hotUpdateMainFilename: 'hot/[hash].hot-update.json'
    }
}

module.exports = [client, server]
