module.exports = api => {
    api.cache(true)
    const presets = [
        ["@babel/preset-env", {
            corejs: {version: 3},
            useBuiltIns: 'usage',
        }],
        "@babel/preset-react",
    ]
    const plugins = [
        "react-hot-loader/babel",
        "@babel/plugin-proposal-export-default-from",
        "@babel/plugin-proposal-class-properties"
    ]
    return {
        presets,
        plugins
    }
}
